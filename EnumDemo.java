package enums;

public class EnumDemo {

	public static void main(String[] args) {
		
		//System.out.println(Colours.BLACK);
		//System.out.println(Days.WEDNESDAY);
		
		Days[] days = Days.values();
		System.out.println(days[2]);
		System.out.println(days.length);
		
		System.out.println(ColdDrink.COKE.name());
		System.out.println(ColdDrink.COKE.getBrandName());
		
		System.out.println(ColdDrink.SPRITE.name());
		System.out.println(ColdDrink.SPRITE.getBrandName());
		
		System.out.println(ColdDrink.PEPSI.name());
		System.out.println(ColdDrink.PEPSI.getBrandName());
		
		
	}

}

