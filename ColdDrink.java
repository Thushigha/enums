package enums;

public enum ColdDrink {
	
	PEPSI("This is a pepsi cola Brand"), COKE("Coca Cola"), SPRITE("Sprite");
	
	private String brandName;
	
	private ColdDrink(String brandName) {
		this.brandName = brandName;
		
	}
	
	public String getBrandName() {
		return brandName;
		
	}

}
